\section{Introduction}



\subsection{How can we know what stars are made of?}

For a long time humans were looking at stars in the night sky and wondering what are those stars made of? During the last two centuries astronomers found that a typical star is a collection of different kinds of atoms. By far the most abundant atom is the lightest one, which contains one proton and represents a compact bundle of energy. This atom contributes about 70\% to the mass of a star \citep{Russell}. The second most abundant element consists of two protons, and it is responsible for approximately 28\% of a star's mass. The remaining 2\% of the mass of a star is made of heavier atoms that have more than two protons. We can determine the types of elements by looking at the light from a star, which is produced by the following process.

The number of atoms in a star is so large that gravity pulls them into the center of the star creating high pressures and temperatures. This makes the lighter atoms transform into heavier ones. During this transformation some of the energy from lighter atoms is then used to create \emph{electromagnetic waves}, also known as \emph{light} \citep{Maxwell01011865}. The light exists both as a wave and as a compact bundle of energy called a \emph{photon}.

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/light_absorption}
  \captionof{figure}{Detection of an atom type by looking at light frequencies. Initially, three light waves having different frequencies approach an atom. One light wave is absorbed and the remaining two waves are detected by an astronomer. Now the astronomer knows that there is a specific type of atom between her and the source of light.}
  \label{fig_light_absoption}
\end{Figure}

A star generates very large number of electromagnetic waves that have different frequencies. When light moves close to an atom in the outer atmosphere of a star the energy of the light can be absorbed by this atom. The key property of nature is that only light that has specific frequency is absorbed \citep{Bohr}, and different types of atoms absorb different frequencies of light. Consequently, the atoms remove some frequencies from the light and let the remaining frequencies reach observers on Earth. By looking at the light from a star we can detect these absent frequencies and find the types of atoms that absorbed them (\autoref{fig_light_absoption}). This allows us to answer the age-old question and find out what elements the stars are made of.




\subsection{How do we use a computer to estimate chemical composition of a star?}

We learned that certain chemical elements remove particular frequencies from the light that we see from stars. Now we can use a computer and create a picture of light from a hypothetical computer-generated star that contains particular elements in its outer atmosphere. Next, we compare this computer-generated picture with the actual light that we see from a real star. If the picture matches the light then we stop and conclude that our virtual star looks like the real one, and that there is a good chance that the virtual and real stars are made of similar atoms. If there is no match, then we create another virtual star containing different elements and repeat the process until we get a reasonable agreement with the light from a real star.

For example, we can ask computer to create a virtual star that contains a particular element that absorbs light at frequency of $\SI{100}{\Hz}$. The computer will then create a picture of light that has the missing frequency, as shown as a dip drawn with solid line on \autoref{matchining_absorption_lines}. The dip is also called the absorption line.

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/matchining_absorption_lines}
  \captionof{figure}{Matching the picture of light from computer generated star (solid line) with the light from a real star (dashed line). The dips represent the frequencies absorbed by atoms located on stellar surfaces. The two dips occur at different frequencies and indicate no match.}
  \label{matchining_absorption_lines}
\end{Figure}

Next, we compare the picture with the light from the real star, which contains a dip shown as a dashed line. Unfortunately for us, the two dips don't match even remotely. This means that our computer model of the star does not represent the real star and we need to try again and ask computer to make a star containing a different element.

Fortunately, if programmed carefully, modern computers can draw pictures of light faster than humans. Besides, computers can even compare those virtual pictures with light from a real star and decide if the absorption lines are close to each other. If they are close, then the computer program stops and declares a victory, as shown on \autoref{matchining_absorption_lines_good_match}. We can then conclude that the chemical elements in our little virtual star produce light that looks similar to the light from a real star.

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/matchining_absorption_lines_good_match}
  \captionof{figure}{A good match between the absorption lines from the virtual (solid line) and real star (dashed line). }
  \label{matchining_absorption_lines_good_match}
\end{Figure}

Does it mean that we discovered the composition of the real star? No it does not. Without actually flying a rocket to the star and taking a sample we can never be sure. All we can do is to create virtual stars and try to make them look like the real ones. The best we can do from Earth is to say that there is a chance that the chemical composition of our virtual star agrees with the real one. But this result, however uncertain, is better than none.


\subsection{How does computer calculates light from a virtual star?}

A light from a star that contains multiple absorption lines is called \emph{spectrum}. In order to calculate such spectrum a computer program needs the following ingredients.


\subsubsection{Model atmosphere}

Firstly, computer needs to know parameters of the atmosphere of the virtual star, which are called \emph{model atmosphere}. Model atmosphere may contain a hundred of layers of the atmosphere. This list starts with layers that are closer to the center of the star and ends with the outer ones. Each layer may contains the following numbers: depth of the layer, temperature, gas, electron and radiation pressure. The model atmosphere is a file that is generated by a program that creates models of stars. In this project we used a model atmosphere from a computer program called MARCS \citep{MARCS}.


\subsubsection{Atomic energy states}

Secondly, computer needs a list of \emph{energy states} for different atoms and molecules, also called \emph{quantum states}. When an atom absorbs a photon, its energy is transferred to the atom. As a result, the atom transitions from \emph{ground} to \emph{excited} state, as shown on \autoref{exciting_an_atom}.

\begin{Figure}
  \centering
  \includegraphics[width=0.5\textwidth]{figures/Exciting_an_atom}
  \captionof{figure}{An atom in its ground energy state absorbs light and transitions to an excited energy state. }
  \label{exciting_an_atom}
\end{Figure}

Each atom can have not just one but multiple excited energy states. If, for example, the photon had higher frequency, then after its absorption the atom will transform into a higher quantum energy state.


\subsubsection{Spectral synthesis program}

Finally, to calculate a stellar spectrum, one needs to write a computer program. The program takes a model atmosphere and energy states as input and produces a list of all the absorption lines as output. This process is called \emph{spectral synthesis}. Commonly used spectral synthesis programs are MOOG \citep{Moog}, Spectroscopy Made Easy \citep{SpectroscopyMadeEasySoftware}, Spectrum \citep{SpectrumSoftware} and Turbospectrum \citep{TurbospectrumSoftware}.


\subsection{How can we improve spectral synthesis programs?}

The estimations of chemical compositions in stars can be improved by using more accurate input data for atomic energy states and model atmospheres. Unfortunately, these data are not always available. The uncertainties for some atomic energy states are too large to be used for high detail spectral synthesis \citep{2016A&ARv..24....9B}. Moreover, data for some atomic energy states are missing altogether. Consequently, the origin of the half of the absorption lines in the solar spectrum is unknown \citep{1990asos.conf...20K}.

In order to improve the spectral synthesis, new methods need to be developed that have smaller dependence on the quality of atomic data. Instead, these methods can deduce the missing or inaccurate atomic data by comparing synthetic and observed spectra and using statistical analysis.



\subsection{What do we want to achieve with this project?}

In this project we look at a spectral synthesis program called MOOG \citep{Moog} that is written in Fortran programming language. We want to analyze the MOOG code and mimic some of it behavior in a Python program we call 'Parrot'. The main goal for us here is to learn how to write astrophysical software by looking at the MOOG's code, which is actively used by astronomers. The smaller sub-goals that we hope to accomplish along the way are the following.



\subsubsection{Making the program run faster}

Currently it takes about an hour to estimate chemical composition of one star with MOOG, and there are spectral data from about $10$ million stars. To process these data in reasonable amount of time we will need to make the Parrot program faster. We anticipate that this could be done by running Parrot code and finding its slow parts. After we identify these performance bottlenecks we can rewrite the code more efficiently. In addition, it may be possible to replace some of the slowest code with a one that runs faster by making it less accurate. In other words, we can gain speed by reducing accuracy, if the end results is still acceptable.

Another way of improving the speed of Parrot is to use parallel execution of code. Today, processors include multiple cores that can run programs simultaneously. We can take advantage of those cores and write our code in such a way that it can perform multiple calculations at the same time.


\subsubsection{Improving accuracy}

The quality of spectral syntheses used in MOOG can be improved by using statistical techniques and solving for the inaccurate or missing atomic data parameters. This can be possible by calculating the derivatives of the result with respect to these parameters. For example, we can calculate by how much the strength of an absorption line changes if we increase an atomic energy level.


\subsubsection{Making our code accessible}

We aim to make our Parrot program easy to install and use by people with little knowledge of programming. We can accomplish this by writing the program in Python language. We choose Python because simplicity and readability are parts of its design principles \citep{zen-of-python}. Another way of making Parrot accessible is to make it open source and available for free on GitHub code hosting service \citep{github}. This will allow anyone to use the program, participate in its development, or use it as a basis for their own software.