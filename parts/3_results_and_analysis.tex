\section{Results and Analysis}



\subsection{Calculating number densities}

During the course of this project we were mostly working on implementing a large subroutine called \code{eqlib} that calculates number densities of atoms and molecules for all layers of the atmosphere. This subroutine contains $290$ lines of code, and it the central subroutine used by many other parts of the program. By the week $8$ of the project we have implemented the subroutine completely in Parrot.

We used to subroutine to plot the partial pressure of molecular hydrogen and water, shown on Figures \ref{fig_partial_pressure_molecular_hydrogen} and \ref{fig_partial_pressure_water}. The graphs show the pressure at different paths in the atmosphere of a stellar model with $\SI{5777}{\K}$ surface temperature and $\num{4.44}$ for the logarithm of the surface gravity.

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/molecular_hydrogen}
  \captionof{figure}{The partial pressure of molecular hydrogen at different depths in the model atmophere of a star.}
  \label{fig_partial_pressure_molecular_hydrogen}
\end{Figure}

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/water}
  \captionof{figure}{The partial pressure of water molecules at different depths in the model atmophere of a star.}
  \label{fig_partial_pressure_water}
\end{Figure}

The most important part of Figures \ref{fig_partial_pressure_molecular_hydrogen} and \ref{fig_partial_pressure_water} is that they show data from both MOOG (dots) and Parrot (solid line). As we can see from the graphs, there is a good agreement between the two results. In fact, the values from MOOG and Parrot matched exactly to all $15$ significant figures. We could not detect any difference between the partial pressures calculated by MOOG and Parrot, within the double precision used in both programs. This is encouraging, since it may indicate that we did not make many mistakes when converting MOOG's code to Parrot.



\subsection{Improving the accuracy of MOOG}

We have managed to improve the accuracy of the MOOG program by removing random noise that was added to some of the numbers used in the code. MOOG already uses double precision type ``\code{real*8}'' for decimal variables. The constant numbers used in MOOG code, however, only set the most significant bytes, while the renaming bytes are filled with random numbers. For example, an \code{xsolar} array shown in \autoref{moog_xsolar} is assigned a list of decimal numbers.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={The declaration of xsolar array in MOOG. File: \code{Batom.f}.},frame=tlrb,label={moog_xsolar}]
data xsolar/
     . 12.00, 10.93, 1.05, 1.38, 2.70/
\end{lstlisting}
\end{minipage}

By default, however, Fortran fills the least significant bytes of \code{xsolar} with random noise, as shown in \autoref{xsolar_noise}.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={The least significant bytes of the numbers in the \code{xsolar} are filled with random noise.},frame=tlrb,label={xsolar_noise}]
12.000000000000000
10.930000305175781
1.0499999523162842
1.3799999952316284
2.7000000476837158
\end{lstlisting}
\end{minipage}

This random noise reduced the accuracy of calculations and resulted in poor agreement of only $4$ significant figures between the same functions in MOOG and Parrot. In addition, the random noise made the results of the MOOG program differ between different runs.

One way of solving the random noise issue was to write \emph{d0} at the end of the numbers, as shown in \autoref{moog_xsolar_fixed}.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={Setting zeros for the least significant figures of the numbers in xsolar array. File: \code{Batom.f}.},frame=tlrb,label={moog_xsolar_fixed}]
data xsolar/
     * 12.00d0, 10.93d0, 1.05d0, 1.38d0/
\end{lstlisting}
\end{minipage}

This approach was not practical for MOOG, because we needed to do this for large number of constants. We have managed to fix the issue by using the compiler flags shown in \autoref{gfortran_flags}.
\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={Flags used in makefile for gfortran compiler that remove the random noise from the constant numbers.},frame=tlrb,label={gfortran_flags}]
-fdefault-real-8 -fdefault-double-8
\end{lstlisting}
\end{minipage}

These flags are specific to the \emph{gfortran} compiler. Therefore, if one decides to build MOOG with \emph{ifort} compiler, we would recommend to apply corresponding compiler flags.



\subsection{Achieving accuracy of $14$ significant figures}

One of the main goals of this project was to make sure that we replicate the behavior of MOOG program as close as possible. So far, we were able to achieve this goal by ensuring that the returned values from \emph{all} the functions agree to at least $14$ significant figures between MOOG and Parrot. Most of the results agreed to $15$ significant figures, which is the maximum accuracy that is possible to achieve from the double precision data types that are used in both MOOG and Parrot.

We used unit tests to verify that the results from MOOG and Parrot's runctions agree with high accuracy. For example, a unit test shown in \autoref{unit_test_accuracy} verifies that the value in the \emph{result} variable, that came from a Parrot function, agrees with the number \emph{92903433.466602683}, that was returned by MOOG, to at least one part per $100$ billion. Such unit test will continue to ensure that the accuracy between results  remains high in the future, as we change and evolve the Parrot project.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={A unit test that ensures that the two values match to at least one part per $100$ trillion},frame=tlrb,label={unit_test_accuracy}]
assert result == approx(92903433.466602683, rel=1e-15, abs=0)
\end{lstlisting}
\end{minipage}



\subsection{Creating unit tests}

During this project we were writing unit tests to ensure that out code behaves exactly as intended. We created unit tests for every Parrot function, $50$ unit test in total.

A unit test for one of the MOOG's functions \emph{dicov} is shown in \autoref{discov_unit_test}. The unit test calls the \emph{discov} function with specific parameters and verifies that its returned value matches the expected number. For example, when the function \emph{discov} is called with two parameters \emph{60808.0} and \emph{8}, the unit test will show a failure, unless the returned value is \emph{2}. Such unit test will make it harder for us to introduce bugs into the code in the future.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={A unit test file for \emph{discov} function that verifies the returned values for different input parameters.},frame=tlrb,label={discov_unit_test}]
import pytest
from pytest import approx
from .discov import discov

def test_discov():
    assert discov(60808.0, 8) == 2
    assert discov(8.1, 8) == 1
    assert discov(808, 1) == 0
\end{lstlisting}
\end{minipage}


\subsection{Current progress and next steps}

The main steps of this project are shown in \autoref{fig_progress_diagram}. The first two steps (model atmosphere and partition functions) were implemented by Andy Casey before the start of this project. The third step (molecular equilibrium) was completed during this project. The remaining steps need to be done in order to produce a synthetic spectrum. The following are short descriptions of individual steps.

\begin{Figure}
  \centering
  \includegraphics[width=0.4\textwidth]{figures/progress_diagram}
  \captionof{figure}{Progress diagram of this project showing its main milestones.}
  \label{fig_progress_diagram}
\end{Figure}


\subsubsection{Model atmosphere}

The Parrot program reads a model atmosphere file named \code{marcs-2011\_m1.0\_t02\_st.pkl} that was produced by MARCS program \citep{MARCS}. The file contains a table that includes the following parameters for each of 56 layers of stellar atmosphere: depth of the layer, temperature, gas, electron and radiation pressure. The model we used during the development of this program corresponds to a star with \SI{5777}{K} surface temperature and \num{4.44} for the logarithm of surface gravity. These data are used by many other functions in Parrot, such as \code{eqlib} function, that calculates number densities of different elements. 



\subsubsection{Partition functions}

A partition function is a function for calculating properties of the system, such as pressure, for different chemical elements.  MOOG program uses two different partition functions. For most elements, MOOG uses a partition function taken from Atlas computer program \citep{Atlas}. For about $15$ elements, however, MOOG instead uses partition function developed by \citet{Irwin_partition_function}. We have implemented both partition functions in Parrot program, and their output for two chemical elements is shown on Figures \ref{fig_partition_function_rb} and \ref{fig_partition_function_er}. The figures show the values of the partition function calculated at different temperatures. We can see the differences between the Irwin and Kurucz partition functions.

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/partition_function_rb_i}
  \captionof{figure}{The output of two partition functions used in MOOG and Parrot for Rb I atom.}
  \label{fig_partition_function_rb}
\end{Figure}

\begin{Figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/partition_function_er_i}
  \captionof{figure}{The output of two partition functions used in MOOG and Parrot for Er I atom.}
  \label{fig_partition_function_er}
\end{Figure}


\subsubsection{Molecular equilibrium}

The molecular equilibrium calculations were done in the \code{eqlib} subroutine in MOOG. This subroutine calculates the partial pressures of different atoms and molecules at different layers in a stellar atmosphere. We have successfully converted the \code{eqlib} function to Parrot, and the results are shown in Figures \ref{fig_partial_pressure_molecular_hydrogen} and \ref{fig_partial_pressure_water}.


\subsubsection{Opacities}

Our next step would be to convert the parts of MOOG where it calculates the opacities for different types of atoms. Opacity describes the amount of light that goes through different layers of a photosphere.


\subsubsection{Contribution functions}

Next, we will need to implement MOOG's code for contribution function, which calculates relative contribution of different layers of stellar photosphere to the absorption line of a specific atom.


\subsubsection{Spectral synthesis}

Our final step would be to calculate a single absorption line for a given model atmosphere.



