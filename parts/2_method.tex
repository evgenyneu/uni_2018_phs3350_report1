\section{Method} \label{sec_method}

\subsection{Simulating a single absorption line}

We begin by writing the part of MOOG's code that creates an absorption line for a single chemical element. The code uses a model atmosphere that contains about 50 layers. In addition, the code uses the data for the chemical composition of the Sun from \citet{SolarAbundancesData} and atomic energy data from \citet{Grigoriev}


\subsection{Writing unit tests}

One important requirement of our Parrot program is to make it work as close to the original MOOG program as possible. In order to accomplish this we use a technique called \emph{unit testing}. This technique includes creating a collection of small functions. Each function is accomplishing a single task. Then, for each of the function we write another function called a ``test'', or ``unit test''. The test runs the original function with different input parameters and compares the returned value with expected one.

For example, suppose we want to write a function called \code{multiply} that takes two numbers and returns their product, as shown in \autoref{function_for_unit_test}.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={Function for multiplying numbers. File: \code{multiply.py}.},frame=tlrb,label={function_for_unit_test}]
def multiply(a, b):
  return a * b
\end{lstlisting}
\end{minipage}

Next, we write a unit test function called \code{test\_multiply} (\autoref{unit_test}). 

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={A unit test that runs the \code{multiply} function with various parameters and makes sure its return values match the expected ones. File: \code{multiply\_test.py}.},frame=tlrb,label={unit_test}]
from multiply import multiply

def test_multiply():
  assert multiply(2, 21) == 42
  assert multiply(2.5, 3) == 7.5
  assert multiply(2, -3) == -6
  assert multiply(0, 7) == 0
\end{lstlisting}
\end{minipage}

Finally, we run the unit test using the ``pytest'' Python library \citep{pytest}. In the output we see if our test succeed or failed.

We believe that the unit tests will make Parrot code more reliable. In the future, when we want to rewrite some of the functions to make them faster, we will still want the functions to return the same results as before. Therefore, the unit tests that we wrote will guard us from making any mistakes and ensure the program continues to work exactly the same way as before.


\subsection{Executing Fortran code from Python unit tests}

In this project we are translating Fortran code to Python. Therefore, it is important to be able to execute the original Fortran subroutines and compare their output with that from functions that we develop in Python. We found a method of calling Fortran functions from Python code, and it can be demonstrated with the following example.

Suppose, we have a Fortran function that receives two integer numbers and returns their product (\autoref{fortran_function_for_converting_to_python}). Our goal is to run this function from a Python unit test and compare its result with the Python function from \autoref{function_for_unit_test}. First, we copy the Fortran function and save it to a separate file named \code{multiply.f90}.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Fortran, caption=A Fortran function that adds two numbers. File: \code{multiply.f90}.,frame=tlrb,label={fortran_function_for_converting_to_python}]
function multiply(a, b) result(result)
  integer     :: a
  integer     :: b
  integer     :: result
  result = a * b
end function multiply
\end{lstlisting}
\end{minipage}

Next, we use the \code{F2PY} program from \code{NumPy} Python package to generate a Python module \citep{f2py}.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Fortran, caption=Generating a Python module from a Fortran function,frame=tlrb,label={create_python_function_from_fortran}]
f2py -c -m multiply_fortran multiply.f90
\end{lstlisting}
\end{minipage}

Finally, we write a unit test that runs both Python and Fortran functions with different arguments and verifies that their output is identical (\autoref{compare_python_and_fortran_functions})


\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption=Running a Fortran function in Python unit test and comparing its output with the one from the Python function. File: \code{multiply\_test.py}.,frame=tlrb,label={compare_python_and_fortran_functions}]
from multiply_fortran import multiply as multiplyf
from multiply import multiply

def test_multiply_fortran():
  assert multiply(2, 3) == multiplyf(2, 3)
  assert multiply(2, -3) == multiplyf(2, -3)
  assert multiply(0, 7) == multiplyf(0, 7)
\end{lstlisting}
\end{minipage}

In order to demonstrate this technique and provide more detailed instructions we created a sample open source project \citep{fortran_form_python}.


\subsection{Chunking: translating a large Fortran subroutines to Python}

From the beginning of this project we encountered a difficulty that comes from the coding style used in the MOOG project. Some MOOG subroutines have many lines of code, and accomplish more than a single task. This made it difficult for us to write unit tests for such subroutines.

For example, a MOOG subroutine called \code{eqlib} that calculates molecular equilibrium contains about $300$ lines of code. The subroutine does many different operations that are hidden inside it and never returned as output parameters. This made it impossible for us to write unit tests and find out what this subroutine was doing. The only data we could get were the the final results it returned. This, however, was not very helpful, because we needed to implement all the internal functionality in Python and make sure it works correctly.

In order to solve this problem we developed a technique that we call \emph{chunking}. It involves breaking original Fortran subroutines into multiple smaller \emph{chunks}. Each chunk needs to accomplish only a single task \citep{single_responsibility}. We found that a chunk that does just one thing was easier to understand and write unit tests for.

In practice, we implement the chunking technique by using the following steps. First we take a long Fortran subroutine that we want to decompose. Then, we create a chunk by copying a small portion of its code into a separate Fortran subroutine or function. Next, we write a unit test in Python where we call this chunk with different input arguments and see what values is returns. Finally, we create a Python function that mimics chunk's behavior by matching its output data for various input arguments.


\subsection{Bundling: removing dependence on global variables}

Another challenge that we faced while writing the Parrot program was the usage of global variables in MOOG code. This design feature of MOOG project has made it difficult for us to run individual MOOG subroutines in unit tests in isolation and understand what they were doing.

A function (also known as subroutine) is a piece of computer code that has a name and returns some data. For example, a subroutine named \code{multiply\_independent} shown in \autoref{independent_subroutine} takes two input arguments $a$ and $b$ and returns their product.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={An independent function for multiplying two numbers. It uses only data supplied in its arguments $a$ and $b$.},frame=tlrb,label={independent_subroutine}]
def multiply_independent(a, b):
  return a * b
\end{lstlisting}
\end{minipage}

The \code{multiply\_independent} is an example of a function that can be used in isolation, because it does not depend on any data apart from its input arguments. Thus, we can easily call this function and understand how it works.

In contrast, the function \code{multiply\_coupled} does not take any input arguments. Instead, it uses \emph{global variables} $a$ and $b$. This function is harder to use because the global variables $a$ and $b$ need to be defined.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Python, caption={A coupled function that returns the products of the global variables $a$ and $b$.},frame=tlrb,label={dependent_subroutine}]
def multiply_coupled():
  return a * b
\end{lstlisting}
\end{minipage}

Unfortunately, we found that many of the MOOG subroutines use global variables that makes it hard to execute them in isolation without running the entire spectral syntheses program. For example, the \code{eqlib} subroutine uses data from global variables for model atmosphere and atomic energy levels. Consequently, we can not create a unit test for this subroutine, because it requires global data to be present.

In order to solve this issue we create an intermediate Fortran file that we call a \emph{bundle} that includes both the global data and the subroutine that we need to test. An example of such bundle is shown in \autoref{a_bundle} and is stored in \code{bundle.f90} Fortran file.

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Fortran, caption={A bundle file that includes both the global data and the subroutine that uses it. File: \code{bundle.f90}},frame=tlrb,label={a_bundle}]
include 'global_data.f90'
include 'subroutine.f90'
\end{lstlisting}
\end{minipage}

Next, we run the \code{F2PY} and convert the bundle file to Python module (\autoref{create_python_function_from_fortran_bundle}).

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[language=Fortran, caption=Generating a Python module from a Fortran bundle,frame=tlrb,label={create_python_function_from_fortran_bundle}]
f2py -c -m bundle_fortran bundle.f90
\end{lstlisting}
\end{minipage}

Finally, we can run the Fortran subroutine that uses global data from Python unit tests and understand what it is doing.


